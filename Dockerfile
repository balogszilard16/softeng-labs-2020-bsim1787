FROM node:14.15.4
WORKDIR /usr/src/softeng
COPY package.json .
RUN npm install

COPY . .
EXPOSE 8080
CMD ["node", "app.js"]